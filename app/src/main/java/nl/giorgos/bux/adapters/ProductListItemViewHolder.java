package nl.giorgos.bux.adapters;

import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import nl.giorgos.bux.R;

public class ProductListItemViewHolder extends RecyclerView.ViewHolder{
    public CardView cardItemLayout;
    public TextView title;

    public ProductListItemViewHolder(View itemView) {
        super(itemView);
        cardItemLayout = (CardView) itemView.findViewById(R.id.cardlist_item);
        title = (TextView) itemView.findViewById(R.id.title);

    }

    public void update(String s){
        title.setText(s);
    }
}
