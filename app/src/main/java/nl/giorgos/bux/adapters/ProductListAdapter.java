package nl.giorgos.bux.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import org.w3c.dom.Text;

import java.util.ArrayList;
import java.util.List;

import nl.giorgos.bux.R;


public class ProductListAdapter extends RecyclerView.Adapter<ProductListItemViewHolder>{
    private static final String TAG = "G_ProductListAdapter";

    List<String> items = new ArrayList<>();

    public ProductListAdapter(List<String> items) {
        this.items = items;
    }

    @Override
    public ProductListItemViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.list_item, viewGroup, false);
        return new ProductListItemViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ProductListItemViewHolder appViewHolder, int i) {
        String t= items.get(i);
        appViewHolder.update(t);
    }

    @Override
    public int getItemCount() {
        return items.size();
    }
}