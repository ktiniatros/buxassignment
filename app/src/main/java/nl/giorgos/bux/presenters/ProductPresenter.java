package nl.giorgos.bux.presenters;

import android.util.Log;

import com.koushikdutta.async.callback.CompletedCallback;
import com.koushikdutta.async.http.AsyncHttpClient;
import com.koushikdutta.async.http.AsyncHttpGet;
import com.koushikdutta.async.http.WebSocket;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.net.SocketException;
import java.text.NumberFormat;
import java.util.Currency;

import nl.giorgos.bux.R;
import nl.giorgos.bux.activities.ProductActivity;
import nl.giorgos.bux.Utilities.Utils;
import nl.giorgos.bux.models.Product;
import nl.giorgos.bux.services.JSONService;
import rx.Observer;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * Created by giorgos on 11/09/16.
 */
public class ProductPresenter {
    private final String TAG = "G_ProductPresenter";

    private String productId, productName = "";

    JSONService jsonService = new JSONService();
    WebSocket currentWebSocket;

    public void setProduct(String productId, String productName) {
        this.productId = productId;
        this.productName = productName;
    }

    public void unload(){
        if(currentWebSocket != null){
            currentWebSocket.close();
            currentWebSocket = null;
        }
    }

    public void pause(){
        if(currentWebSocket != null){
            currentWebSocket.pause();
        }
    }

    public void update(final ProductActivity productActivity) {
        if(currentWebSocket != null && currentWebSocket.isPaused()){
            currentWebSocket.resume();
            return;
        }

        if(productId == null){
            Log.w(TAG, "Please provide a product id!");
            return;
        }

        productActivity.setTitle(productName);

        jsonService.getApi().getProduct(productId).subscribeOn(Schedulers.newThread()).observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<Product>() {
                    @Override
                    public void onCompleted() {
                        Log.d(TAG, "onCompleted");
                    }

                    @Override
                    public void onError(Throwable e) {
                        productActivity.onError(e.getMessage());
                    }

                    @Override
                    public void onNext(Product p) {
                        updateCurrentPrice(p, productActivity);
                    }
                });
    }

    private void updateCurrentPrice(Product p, final ProductActivity productActivity){
        final String uri = Utils.SubscriptionsURL;
        final AsyncHttpGet request = new AsyncHttpGet(uri);
        request.addHeader("Authorization", "Bearer " + Utils.TOKEN);
        request.addHeader("Accept", "application/json");
        request.addHeader("Accept-Language", "nl-NL,en;q=0.8");

        final String currency = p.getQuoteCurrency();
        final String yesterdayPrice = p.getClosingPrice().getAmount();
        final int decimals = p.getDisplayDecimals();
        NumberFormat nf = NumberFormat.getCurrencyInstance();
        nf.setCurrency(Currency.getInstance(currency));
        nf.setMaximumFractionDigits(decimals);
        nf.setMinimumFractionDigits(decimals);
        final double yesterdayValue = Double.parseDouble(yesterdayPrice);
        final String yesterdayPriceToShow = nf.format(yesterdayValue);
        productActivity.updateYesterdayPrice(yesterdayPriceToShow);
        AsyncHttpClient.getDefaultInstance().websocket(request, null, new AsyncHttpClient.WebSocketConnectCallback() {
            @Override
            public void onCompleted(Exception ex, final WebSocket webSocket) {
                currentWebSocket = webSocket;
                if (ex != null) {
                    ex.printStackTrace();
                    productActivity.onError(ex.getMessage());
                    return;
                }

                webSocket.setClosedCallback(new CompletedCallback() {
                    @Override
                    public void onCompleted(Exception ex) {
                        if(ex instanceof SocketException){
                            productActivity.onError(productActivity.getString(R.string.NetworkError));
                        }
                    }
                });

                webSocket.setStringCallback(new WebSocket.StringCallback() {
                    @Override
                    public void onStringAvailable(String s) {
//                        Log.d(TAG, "onStringAvailable " + s);
                        JSONObject response = Utils.parse(s);
                        JSONObject body = Utils.getJsonFromJson(response, "body");
                        String messageType = Utils.getStringFromJson(response, Utils.WSType);

                        switch(messageType){
                            case Utils.WSConnectionError:
                                //in case of error, inform user
                                final String message = Utils.getStringFromJson(body, Utils.WSErrorCode);
                                productActivity.onError(message);
                                break;
                            case Utils.WSConnectionSuccess:
                                //in case of success, continue communication, get updates etc
                                JSONObject subscribeObject = new JSONObject();
                                JSONArray products = new JSONArray();
                                products.put("trading.product." + productId);
                                try{
                                    subscribeObject.put("subscribeTo", products);
                                }catch(JSONException exc){
                                    exc.printStackTrace();
                                }
                                webSocket.send(subscribeObject.toString());
                                break;
                            case Utils.WSTradingQuote:
                                //got update for a stock price
                                final double parsed = Double.parseDouble(Utils.getStringFromJson(body, Utils.CurrentPrice));
                                NumberFormat nf = NumberFormat.getCurrencyInstance();
                                nf.setCurrency(Currency.getInstance(currency));
                                nf.setMinimumFractionDigits(decimals);
                                nf.setMaximumFractionDigits(decimals);
                                String formatted = nf.format(parsed);

                                final String currentPrice = formatted;
                                final String sign = parsed > yesterdayValue ? "+" : (parsed < yesterdayValue ? "-" : "");

                                final NumberFormat percentageFormat = NumberFormat.getInstance();
                                percentageFormat.setMinimumFractionDigits(Utils.PERCENTAGE_DECIMALS);
                                percentageFormat.setMaximumFractionDigits(Utils.PERCENTAGE_DECIMALS);

                                String perceDiff = sign + percentageFormat.format(1 - Math.min(yesterdayValue, parsed) / Math.max(yesterdayValue, parsed)) + "%";
                                productActivity.updateCurrentPriceAndDifference(currentPrice, perceDiff);
                                break;
                            default:
                                Log.d(TAG, messageType + ": " + "UnsupportedOperation");
                                break;
                        }
                    }
                });
            }
        });
    }
}
