package nl.giorgos.bux.presenters;

import java.util.ArrayList;
import java.util.HashMap;

import nl.giorgos.bux.interfaces.ProductsUpdate;

/**
 * Created by giorgos on 11/09/16.
 *
 */
public class ProductListPresenter {
    private final String TAG = "G_ProductListPresenter";

    private final HashMap<String, String> stocks = new HashMap<>();

    public ArrayList<String> getNames() {
        return names;
    }

    public HashMap<String, String> getStocks() {
        return stocks;
    }

    private ArrayList<String> names = new ArrayList<>();

    public ProductListPresenter(){
        stocks.put("BTC/USD", "28625");
        stocks.put("EUR/GBP", "26620");
        stocks.put("Germany 30", "26609");
        stocks.put("Spain 35", "26610");
        stocks.put("Apple", "26629");
        stocks.put("Facebook", "26627");

        for(String s : stocks.keySet()){
            names.add(s);
        }
    }

    public void update(ProductsUpdate pUpdate){
        pUpdate.update(names);
    }

}
