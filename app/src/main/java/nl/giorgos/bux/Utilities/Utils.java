package nl.giorgos.bux.Utilities;

import android.util.Log;
import android.widget.TextView;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by giorgos on 09/09/16.
 * Contains utility methods but also some constant values and settings
 */
public class Utils {
    private static final String TAG = "G_Utils";

    //some constants..
    public static final String WSErrorCode = "errorCode";
    public static final String WSConnectionSuccess = "connect.connected";
    public static final String WSConnectionError = "connect.failed";
    public static final String WSTradingQuote = "trading.quote";
    public static final String WSType = "t";
    public static final String CurrentPrice = "currentPrice";

    public static final String SubscriptionsURL = "https://rtf.dev.getbux.com/subscriptions/me";
    public static final String TOKEN = "eyJhbGciOiJIUzI1NiJ9.eyJyZWZyZXNoYWJsZSI6ZmFsc2UsInN1YiI6IjczYzhmMDVlLWMwNzktNGNiYS04ODI4LTJlYjJkZjQ4NGZkNCIsImF1ZCI6ImRldi5nZXRidXguY29tIiwic2NwIjpbImFwcDpsb2dpbiIsInJ0Zjpsb2dpbiJdLCJuYmYiOjE0NjQxMDM4NDAsImV4cCI6MTQ5NTYzOTg0MCwiaWF0IjoxNDY0MTAzODQwLCJqdGkiOiIyZTYzODBiOC01OGUzLTRjOWItYjlmYi01MjUxMmNmZjFlMzUiLCJjaWQiOiI4NDczNjIyOTMzIn0.XbbTKek4Q5eJ27moVCakXjS_dXMYGccFTukZTtpLEsk";

    public static final int PERCENTAGE_DECIMALS = 4;

    public static JSONObject parse(String s){
        String TAG = Utils.TAG + "_parse";
        JSONObject json = new JSONObject();
        try{
            json = new JSONObject(s);
        }catch(JSONException exc){
            Log.d(TAG, "Parse exception: ");
            exc.printStackTrace();
        }
        return json;
    }

    public static String getStringFromJson(JSONObject json, String parameter){
//        String s = "";
//        String TAG = Utils.TAG + "_getStringFromJson";
//        try{
//            s = json.getString(parameter);
//        }catch(JSONException exc){
//            Log.d(TAG, "getStringFromJson exception: ");
//            exc.printStackTrace();
//        }
//        return s;
        return json.optString(parameter);
    }

    public static JSONObject getJsonFromJson(JSONObject json, String parameter){
        JSONObject jsonjson = new JSONObject();
        String TAG = Utils.TAG + "_getJsonFromJson";
        try{
            jsonjson = json.getJSONObject(parameter);
        }catch(JSONException exc){
            Log.d(TAG, "getJsonFromJson exception: ");
            exc.printStackTrace();
        }
        return jsonjson;
    }


    public static void updateTextWithAnimation(final TextView text, final String s){
        text.animate().setDuration(100).scaleX(0).scaleY(0).withEndAction(new Runnable() {
            @Override
            public void run() {
                text.setText(s);
                text.animate().setDuration(200).scaleX(1f).scaleY(1f).start();
            }
        }).start();
    }
}
