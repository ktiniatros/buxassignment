package nl.giorgos.bux.services;

import android.util.Log;

import java.net.SocketTimeoutException;

import nl.giorgos.bux.Utilities.Utils;
import nl.giorgos.bux.interfaces.JSONApi;
import retrofit.ErrorHandler;
import retrofit.RequestInterceptor;
import retrofit.RestAdapter;
import retrofit.RetrofitError;

/**
 * Created by giorgos on 09/09/16.
 */
public class JSONService {
    private static final String TAG = "G_JSONService";
    private static final String BASE_URL = "https://api.dev.getbux.com";

    private JSONApi api;

    public JSONService(){
        RequestInterceptor requestInterceptor = new RequestInterceptor() {
            @Override
            public void intercept(RequestFacade request) {
                request.addHeader("Authorization", "Bearer " + Utils.TOKEN);
                request.addHeader("Accept", "application/json");
                request.addHeader("Accept-Language", "nl-NL,en;q=0.8");
            }
        };

        RestAdapter restAdapter = new RestAdapter.Builder()
                .setEndpoint(BASE_URL)
                .setRequestInterceptor(requestInterceptor)
//                .setLogLevel(RestAdapter.LogLevel.FULL)
                .setErrorHandler(new ErrorHandler() {
                    @Override
                    public Throwable handleError(RetrofitError cause) {
                        Log.d(TAG, cause.getMessage());
                        Exception exc = new Exception("Unknown Server Error. We will look into it!");

                        switch(cause.getKind()){
                            case NETWORK:
                                exc = new Exception("Please check your Internet connection");
                                break;
                            case HTTP:
                                return cause;

                        }
                        return exc;
                    }
                })
                .build();



        api = restAdapter.create(JSONApi.class);
    }

    public JSONApi getApi(){
        return api;
    }
}
