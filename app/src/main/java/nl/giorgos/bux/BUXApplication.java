package nl.giorgos.bux;

import android.app.Activity;
import android.app.Application;
import android.content.Intent;
import android.os.Bundle;

import nl.giorgos.bux.activities.ProductActivity;
import nl.giorgos.bux.interfaces.ActivityClicker;
import nl.giorgos.bux.interfaces.ProductsUpdate;
import nl.giorgos.bux.presenters.ProductListPresenter;
import nl.giorgos.bux.presenters.ProductPresenter;

/**
 * Created by giorgos on 09/09/16.
 *
 * Overviewing here all the application flow. In general, using MPV is quite suitable for android
 * because of the nature of the activities. Makes Testing isolated components easier.
 * This app/assignment is too small for showing the advantages of MPV and in this case it maybe makes
 * it seem too complicated, but still it is a good way to show how it can be organized.
 * Models were created automatically based on the JSON response from the api.
 * The presenters are in charge of retrieving data from models and formatting/filtering them before
 * passing them to the activities, in order to show them in the ui. Activities are left only with
 * their stuff and just receive absolutely necessary data show and make user actions known when needed
 */
public class BUXApplication extends Application implements ActivityClicker {
    private static final String TAG = "G_BUXApplication";
    private final ProductListPresenter productListPresenter = new ProductListPresenter();
    private final ProductPresenter productPresenter = new ProductPresenter();

    public void click(int position, Activity activity){
        String name = productListPresenter.getNames().get(position);
        productPresenter.setProduct(productListPresenter.getStocks().get(name), name);
        Intent intent = new Intent(activity, ProductActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);
    }

    @Override
    public void onCreate() {
        super.onCreate();
        registerActivityLifecycleCallbacks(new ActivityLifecycleCallbacks() {
            @Override
            public void onActivityCreated(Activity activity, Bundle savedInstanceState) {

            }

            @Override
            public void onActivityStarted(Activity activity) {

            }

            @Override
            public void onActivityResumed(Activity activity) {
                if(activity instanceof ProductsUpdate){
                    productListPresenter.update((ProductsUpdate)activity);
                }

                if(activity instanceof ProductActivity){
                    productPresenter.update((ProductActivity)activity);
                }
            }

            @Override
            public void onActivityPaused(Activity activity) {
                //no need to get updates when the user is not focused there
                if(activity instanceof ProductActivity){
                    productPresenter.pause();
                }
            }

            @Override
            public void onActivityStopped(Activity activity) {

            }

            @Override
            public void onActivitySaveInstanceState(Activity activity, Bundle outState) {

            }

            @Override
            public void onActivityDestroyed(Activity activity) {
                if(activity instanceof ProductActivity){
                    productPresenter.unload();
                }
            }
        });
    }
}
