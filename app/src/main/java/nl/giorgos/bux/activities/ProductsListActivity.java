package nl.giorgos.bux.activities;

import android.app.Activity;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import java.util.ArrayList;

import nl.giorgos.bux.R;
import nl.giorgos.bux.Utilities.RecyclerItemClickListener;
import nl.giorgos.bux.adapters.ProductListAdapter;
import nl.giorgos.bux.interfaces.ActivityClicker;
import nl.giorgos.bux.interfaces.ProductsUpdate;

public class ProductsListActivity extends AppCompatActivity implements ProductsUpdate{
    private final String TAG = "G_ProductsList";

    RecyclerView list;
    ProductListAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_products_list);

        list = (RecyclerView)findViewById(R.id.list);

        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getBaseContext());
        list.setLayoutManager(linearLayoutManager);
        list.setHasFixedSize(true);

        final Activity me = this;
        list.addOnItemTouchListener(
                new RecyclerItemClickListener(this, list ,new RecyclerItemClickListener.OnItemClickListener() {
                    @Override public void onItemClick(View view, int position) {
                        ((ActivityClicker)getApplication()).click(position, me);
                    }

                    @Override public void onLongItemClick(View view, int position) {

                    }
                })
        );
    }

    public void update(ArrayList<String> names){
        adapter = new ProductListAdapter(names);
        list.setAdapter(adapter);
    }

}
