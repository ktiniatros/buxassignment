package nl.giorgos.bux.activities;

import android.content.Intent;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import nl.giorgos.bux.R;

public class SplashActivity extends AppCompatActivity {
    private final int splashDuration = 1000;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_splash);

        final SplashActivity me = this;
        new Handler(this.getMainLooper()).postDelayed(new Runnable() {
            @Override
            public void run() {
                startActivity(new Intent(me, ProductsListActivity.class));
                finish();
            }
        }, splashDuration);
    }

}
