package nl.giorgos.bux.activities;

import android.content.DialogInterface;
import android.os.Handler;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.TextView;

import nl.giorgos.bux.R;
import nl.giorgos.bux.Utilities.Utils;

public class ProductActivity extends AppCompatActivity {
    private static final String TAG = "G_MainActivity";

    private TextView yesterdayPriceView, currentPriceView, differenceView;
    private ProgressBar progressBar;
    private Handler mainHandler;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        yesterdayPriceView = (TextView)findViewById(R.id.yesterdayPrice);
        currentPriceView = (TextView)findViewById(R.id.currentPrice);
        differenceView = (TextView)findViewById(R.id.difference);
        progressBar = (ProgressBar)findViewById(R.id.progressBar);
        mainHandler = new Handler(getMainLooper());
    }

    public void onError(final String message){
        final ProductActivity me = this;
        Runnable runnableForAlertDialog = new Runnable() {
            @Override
            public void run() {
                new AlertDialog.Builder(me).setMessage(message).setOnCancelListener(new DialogInterface.OnCancelListener() {
                    @Override
                    public void onCancel(DialogInterface dialog) {
                        finish();
                    }
                }).show();
            }
        };
        mainHandler.post(runnableForAlertDialog);
    }

    public void updateYesterdayPrice(String price){
        yesterdayPriceView.setText(price);
        progressBar.setVisibility(View.GONE);
    }

    public void updateCurrentPriceAndDifference(final String currentPrice, final String difference){
        Runnable runnableForUIUpdate = new Runnable() {
            @Override
            public void run() {
                Utils.updateTextWithAnimation(differenceView, difference);
                Utils.updateTextWithAnimation(currentPriceView, currentPrice);
            }
        };
        mainHandler.post(runnableForUIUpdate);
    }
}
