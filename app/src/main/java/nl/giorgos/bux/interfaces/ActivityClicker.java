package nl.giorgos.bux.interfaces;

import android.app.Activity;

/**
 * Created by giorgos on 11/09/16.
 *
 * Used to detect clicks (and other events eventually) of activities
 */
public interface ActivityClicker {
    void click(int position, Activity activity);
}