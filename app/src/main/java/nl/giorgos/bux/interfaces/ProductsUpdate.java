package nl.giorgos.bux.interfaces;

import java.util.ArrayList;

/**
 * Created by giorgos on 11/09/16.
 */
public interface ProductsUpdate {
    void update(ArrayList<String> productNames);
}
