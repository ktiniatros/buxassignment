package nl.giorgos.bux.interfaces;

/**
 * Created by giorgos on 09/09/16.
 */

import nl.giorgos.bux.models.Product;
import retrofit.http.Path;
import rx.Observable;
import retrofit.http.GET;

/**
 * Created by giorgos on 25/07/16.
 */
public interface JSONApi {

    @GET("/core/8/products/{productId}")
    Observable<Product> getProduct(@Path("productId") String productId);

}
