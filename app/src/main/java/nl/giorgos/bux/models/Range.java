package nl.giorgos.bux.models;

/**
 * Created by giorgos on 09/09/16.
 */
import java.util.HashMap;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "currency",
        "decimals",
        "high",
        "low"
})
public class Range {

    @JsonProperty("currency")
    private String currency;
    @JsonProperty("decimals")
    private Integer decimals;
    @JsonProperty("high")
    private String high;
    @JsonProperty("low")
    private String low;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    /**
     *
     * @return
     * The currency
     */
    @JsonProperty("currency")
    public String getCurrency() {
        return currency;
    }

    /**
     *
     * @param currency
     * The currency
     */
    @JsonProperty("currency")
    public void setCurrency(String currency) {
        this.currency = currency;
    }

    /**
     *
     * @return
     * The decimals
     */
    @JsonProperty("decimals")
    public Integer getDecimals() {
        return decimals;
    }

    /**
     *
     * @param decimals
     * The decimals
     */
    @JsonProperty("decimals")
    public void setDecimals(Integer decimals) {
        this.decimals = decimals;
    }

    /**
     *
     * @return
     * The high
     */
    @JsonProperty("high")
    public String getHigh() {
        return high;
    }

    /**
     *
     * @param high
     * The high
     */
    @JsonProperty("high")
    public void setHigh(String high) {
        this.high = high;
    }

    /**
     *
     * @return
     * The low
     */
    @JsonProperty("low")
    public String getLow() {
        return low;
    }

    /**
     *
     * @param low
     * The low
     */
    @JsonProperty("low")
    public void setLow(String low) {
        this.low = low;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}
