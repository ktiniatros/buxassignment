package nl.giorgos.bux.models;

/**
 * Created by giorgos on 09/09/16.
 */
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "timezone",
        "weekDays"
})
public class OpeningHours {

    @JsonProperty("timezone")
    private String timezone;
    @JsonProperty("weekDays")
    private List<List<WeekDay>> weekDays = new ArrayList<List<WeekDay>>();
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    /**
     *
     * @return
     * The timezone
     */
    @JsonProperty("timezone")
    public String getTimezone() {
        return timezone;
    }

    /**
     *
     * @param timezone
     * The timezone
     */
    @JsonProperty("timezone")
    public void setTimezone(String timezone) {
        this.timezone = timezone;
    }

    /**
     *
     * @return
     * The weekDays
     */
    @JsonProperty("weekDays")
    public List<List<WeekDay>> getWeekDays() {
        return weekDays;
    }

    /**
     *
     * @param weekDays
     * The weekDays
     */
    @JsonProperty("weekDays")
    public void setWeekDays(List<List<WeekDay>> weekDays) {
        this.weekDays = weekDays;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}
