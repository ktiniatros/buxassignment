package nl.giorgos.bux.models;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "symbol",
        "displayName",
        "securityId",
        "quoteCurrency",
        "displayDecimals",
        "maxLeverage",
        "multiplier",
        "currentPrice",
        "closingPrice",
        "dayRange",
        "yearRange",
        "openingHours",
        "category",
        "favorite",
        "productMarketStatus",
        "tags"
})
public class Product {

    @JsonProperty("symbol")
    private String symbol;
    @JsonProperty("displayName")
    private String displayName;
    @JsonProperty("securityId")
    private String securityId;
    @JsonProperty("quoteCurrency")
    private String quoteCurrency;
    @JsonProperty("displayDecimals")
    private Integer displayDecimals;
    @JsonProperty("maxLeverage")
    private Integer maxLeverage;
    @JsonProperty("multiplier")
    private Integer multiplier;
    @JsonProperty("currentPrice")
    private Price currentPrice;
    @JsonProperty("closingPrice")
    private Price closingPrice;
    @JsonProperty("dayRange")
    private Range dayRange;
    @JsonProperty("yearRange")
    private Range yearRange;
    @JsonProperty("openingHours")
    private OpeningHours openingHours;
    @JsonProperty("category")
    private String category;
    @JsonProperty("favorite")
    private Boolean favorite;
    @JsonProperty("productMarketStatus")
    private String productMarketStatus;
    @JsonProperty("tags")
    private List<String> tags = new ArrayList<String>();
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    /**
     *
     * @return
     * The symbol
     */
    @JsonProperty("symbol")
    public String getSymbol() {
        return symbol;
    }

    /**
     *
     * @param symbol
     * The symbol
     */
    @JsonProperty("symbol")
    public void setSymbol(String symbol) {
        this.symbol = symbol;
    }

    /**
     *
     * @return
     * The displayName
     */
    @JsonProperty("displayName")
    public String getDisplayName() {
        return displayName;
    }

    /**
     *
     * @param displayName
     * The displayName
     */
    @JsonProperty("displayName")
    public void setDisplayName(String displayName) {
        this.displayName = displayName;
    }

    /**
     *
     * @return
     * The securityId
     */
    @JsonProperty("securityId")
    public String getSecurityId() {
        return securityId;
    }

    /**
     *
     * @param securityId
     * The securityId
     */
    @JsonProperty("securityId")
    public void setSecurityId(String securityId) {
        this.securityId = securityId;
    }

    /**
     *
     * @return
     * The quoteCurrency
     */
    @JsonProperty("quoteCurrency")
    public String getQuoteCurrency() {
        return quoteCurrency;
    }

    /**
     *
     * @param quoteCurrency
     * The quoteCurrency
     */
    @JsonProperty("quoteCurrency")
    public void setQuoteCurrency(String quoteCurrency) {
        this.quoteCurrency = quoteCurrency;
    }

    /**
     *
     * @return
     * The displayDecimals
     */
    @JsonProperty("displayDecimals")
    public Integer getDisplayDecimals() {
        return displayDecimals;
    }

    /**
     *
     * @param displayDecimals
     * The displayDecimals
     */
    @JsonProperty("displayDecimals")
    public void setDisplayDecimals(Integer displayDecimals) {
        this.displayDecimals = displayDecimals;
    }

    /**
     *
     * @return
     * The maxLeverage
     */
    @JsonProperty("maxLeverage")
    public Integer getMaxLeverage() {
        return maxLeverage;
    }

    /**
     *
     * @param maxLeverage
     * The maxLeverage
     */
    @JsonProperty("maxLeverage")
    public void setMaxLeverage(Integer maxLeverage) {
        this.maxLeverage = maxLeverage;
    }

    /**
     *
     * @return
     * The multiplier
     */
    @JsonProperty("multiplier")
    public Integer getMultiplier() {
        return multiplier;
    }

    /**
     *
     * @param multiplier
     * The multiplier
     */
    @JsonProperty("multiplier")
    public void setMultiplier(Integer multiplier) {
        this.multiplier = multiplier;
    }

    /**
     *
     * @return
     * The currentPrice
     */
    @JsonProperty("currentPrice")
    public Price getCurrentPrice() {
        return currentPrice;
    }

    /**
     *
     * @param currentPrice
     * The currentPrice
     */
    @JsonProperty("currentPrice")
    public void setCurrentPrice(Price currentPrice) {
        this.currentPrice = currentPrice;
    }

    /**
     *
     * @return
     * The closingPrice
     */
    @JsonProperty("closingPrice")
    public Price getClosingPrice() {
        return closingPrice;
    }

    /**
     *
     * @param closingPrice
     * The closingPrice
     */
    @JsonProperty("closingPrice")
    public void setClosingPrice(Price closingPrice) {
        this.closingPrice = closingPrice;
    }

    /**
     *
     * @return
     * The dayRange
     */
    @JsonProperty("dayRange")
    public Range getDayRange() {
        return dayRange;
    }

    /**
     *
     * @param dayRange
     * The dayRange
     */
    @JsonProperty("dayRange")
    public void setDayRange(Range dayRange) {
        this.dayRange = dayRange;
    }

    /**
     *
     * @return
     * The yearRange
     */
    @JsonProperty("yearRange")
    public Range getYearRange() {
        return yearRange;
    }

    /**
     *
     * @param yearRange
     * The yearRange
     */
    @JsonProperty("yearRange")
    public void setYearRange(Range yearRange) {
        this.yearRange = yearRange;
    }

    /**
     *
     * @return
     * The openingHours
     */
    @JsonProperty("openingHours")
    public OpeningHours getOpeningHours() {
        return openingHours;
    }

    /**
     *
     * @param openingHours
     * The openingHours
     */
    @JsonProperty("openingHours")
    public void setOpeningHours(OpeningHours openingHours) {
        this.openingHours = openingHours;
    }

    /**
     *
     * @return
     * The category
     */
    @JsonProperty("category")
    public String getCategory() {
        return category;
    }

    /**
     *
     * @param category
     * The category
     */
    @JsonProperty("category")
    public void setCategory(String category) {
        this.category = category;
    }

    /**
     *
     * @return
     * The favorite
     */
    @JsonProperty("favorite")
    public Boolean getFavorite() {
        return favorite;
    }

    /**
     *
     * @param favorite
     * The favorite
     */
    @JsonProperty("favorite")
    public void setFavorite(Boolean favorite) {
        this.favorite = favorite;
    }

    /**
     *
     * @return
     * The productMarketStatus
     */
    @JsonProperty("productMarketStatus")
    public String getProductMarketStatus() {
        return productMarketStatus;
    }

    /**
     *
     * @param productMarketStatus
     * The productMarketStatus
     */
    @JsonProperty("productMarketStatus")
    public void setProductMarketStatus(String productMarketStatus) {
        this.productMarketStatus = productMarketStatus;
    }

    /**
     *
     * @return
     * The tags
     */
    @JsonProperty("tags")
    public List<String> getTags() {
        return tags;
    }

    /**
     *
     * @param tags
     * The tags
     */
    @JsonProperty("tags")
    public void setTags(List<String> tags) {
        this.tags = tags;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}